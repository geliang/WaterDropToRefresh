package com.qpath;

import java.util.Random;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Scroller;

/**
 * 赛贝尔曲线
 * 
 * @author geliang
 * @warn 无法抗锯齿
 * 
 */

public class MySurfaceView extends SurfaceView implements Callback, Runnable {
	private SurfaceHolder sfh;
	private Paint paint;
	private Thread th;
	private boolean flag;
	private Canvas canvas;
	public static int screenW, screenH;
	// -----------以上是SurfaceView游戏框架
	// 贝赛尔曲线成员变量(起始点，控制（操作点），终止点，3点坐标)
	private int startX = 160, startY = 0, controlX, controlY, endX, endY;
	private int control2X, control2Y;
	// Path
	private Path path;
	private Path path2;
	private Path path_bg;
	// 为了不影响主画笔，这里绘制贝赛尔曲线单独用一个新画笔
	private Paint paintQ;
	// 随机库（让贝赛尔曲线更明显）
	private Random random;
	Scroller scroller;
	private int color_water = 0xFF28A3E6;
	private int color_bg = Color.WHITE;
	private int color_progress = 0x22FFFFFF;

	/**
	 * SurfaceView初始化函数
	 */
	public MySurfaceView(Context context) {
		super(context);
		sfh = this.getHolder();
		sfh.addCallback(this);
		paint = new Paint();
		paint.setColor(color_water);
		paint.setStyle(Style.FILL);
		paint.setAntiAlias(true);

		// // 设置光源的方向
		// float[] direction = new float[]{ 1, 1, 1 };
		//
		// //设置环境光亮度
		// float light = 0.4f;
		//
		// // 选择要应用的反射等级
		// float specular = 6;
		//
		// // 向mask应用一定级别的模糊
		// float blur = 3.5f;
		//
		// EmbossMaskFilter emboss=new
		// EmbossMaskFilter(direction,light,specular,blur);
		//
		// // 应用mask
		// paint.setMaskFilter(emboss);
		setFocusable(true);

		// -----------以上是SurfaceView游戏框架
		// 贝赛尔曲线相关初始化
		path = new Path();
		path2 = new Path();
		path_bg = new Path();
		paintQ = new Paint();
		paintQ.setAntiAlias(true);

		// paintQ.setPathEffect(new DashPathEffect(new float[]{100f,100f},
		// 100f));
		paintQ.setDither(true);
		paintQ.setStyle(Style.FILL);
		paintQ.setStrokeWidth(0);
		paintQ.setColor(color_water);
		setModelingShadowLayer(paintQ, 5, 0, 1, 0xFFFFFFFF);
		random = new Random();
		scroller = new Scroller(context);

		if (Build.VERSION.SDK_INT > VERSION_CODES.HONEYCOMB) {
			setLayerType(View.LAYER_TYPE_HARDWARE, null);
		}
	}

	private void setModelingShadowLayer(Paint p, float radius, float dx,
			float dy, int color) {
		if (Build.VERSION.SDK_INT > VERSION_CODES.HONEYCOMB) {
			// p.setShadowLayer(radius, dx, dy, color);
		}

	}

	/**
	 * SurfaceView视图创建，响应此函数
	 */
	public void surfaceCreated(SurfaceHolder holder) {
		screenW = this.getWidth();
		screenH = this.getHeight();
		flag = true;
		// 实例线程
		th = new Thread(this);
		// 启动线程
		th.start();
		// -----------以上是SurfaceView游戏框架
	}

	/**
	 * 游戏绘图
	 */
	public void myDraw() {
		try {

			canvas = sfh.lockCanvas();
			canvas.setDrawFilter(new PaintFlagsDrawFilter(0,
					Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG));
			if (canvas != null) {
				canvas.drawColor(color_bg);
				// -----------以上是SurfaceView游戏框架

				drawQpath(canvas);
				drawDrop(canvas);
				drawWater(canvas);
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (canvas != null)
				sfh.unlockCanvasAndPost(canvas);
		}
	}

	/**
	 * 源头的水滴的半径
	 */
	int r = 10;

	/**
	 * 绘制源头的水
	 * 
	 * @param canvas2
	 */
	private void drawWater(Canvas canvas2) {

		// canvas.drawCircle(startX, startY+40,60,paint);
		canvas.drawCircle(160, 160, 30, paint);
		/* 画一个空心椭圆形 */

		RectF re = new RectF(startX - r, 0, startX + r, r * 2);
		// RectF re=new RectF(startX-r,0,startX+r,r*2+endY/10);
		canvas.drawOval(re, paint);

		paint.setColor(color_progress);
		setModelingShadowLayer(paint, 5, 0, -3, color_progress);
		canvas.drawArc(re, 0 + r_drop * 4 - 90, r_drop * 12, true, paint);
		paint.setColor(color_water);
		// paint.setShadowLayer(5, 0, -3, 0x88000000);
		setModelingShadowLayer(paint, 5, 0, -3, 0x88000000);
	}

	int dropY = 0;
	int r_drop;

	/**
	 * 绘制滴下去的水
	 * 
	 * @param canvas2
	 */
	private void drawDrop(Canvas canvas2) {
		canvas.drawCircle(160, dropY, r_drop, paint);

	}

	/**
	 * 绘制贝赛尔曲线
	 * 
	 * @param canvas
	 *            主画布
	 */
	public void drawQpath(Canvas canvas) {

		paintQ.setColor(color_water);
		// paintQ.setShadowLayer( 30 , 2,2,0x00ff0000);
		int offset = endY / 10;
		path_bg.reset();
		path_bg.moveTo(startX - r, r);
		path_bg.lineTo(startX + r, r);
		path_bg.lineTo(startX + (r_drop), dropY);
		path_bg.lineTo(startX - (r_drop), dropY);
		canvas.drawPath(path_bg, paintQ);

		// Xfermode xFermode = new PorterDuffXfermode(PorterDuff.Mode.DST_OUT);
		// paintQ.setXfermode(xFermode);
		paintQ.setColor(color_bg);
		// xFermode = new PorterDuffXfermode(PorterDuff.Mode.DST_OVER);
		// paintQ.setXfermode(xFermode);
		// paintQ.setShadowLayer( 30,1,1,0x88000000);
		path.reset();// 重置path
		// 贝赛尔曲线的起始点
		path.moveTo(startX - r, r);
		// 设置贝赛尔曲线的操作点以及终止点
		path.quadTo(startX - r + offset, r + endY / 2, startX - (r_drop), dropY);
		// path.cubicTo(startX-r+offset, r, 160 - 20, endY - 80, 160,
		// 200 + endY / 10);
		// 绘制贝赛尔曲线（Path）
		canvas.drawPath(path, paintQ);

		path2.reset();// 重置path
		// 贝赛尔曲线的起始点
		path2.moveTo(startX + r, r);
		// 设置贝赛尔曲线的操作点以及终止点
		path2.quadTo(startX + r - offset, r + endY / 2, startX + (r_drop),
				dropY);
		// path2.cubicTo(280, 60 + endY / 3, 160 + 20, endY - 80, 160,
		// 200 + endY / 10);
		// 绘制贝赛尔曲线（Path）
		canvas.drawPath(path2, paintQ);

		canvas.drawLine(startX + r, r, startX + (r_drop), dropY, paintQ);
		canvas.drawLine(startX - r, r, startX - (r_drop), dropY, paintQ);

	}

	@Override
	public void computeScroll() {
		// TODO Auto-generated method stub
		super.computeScroll();

		if (scroller.computeScrollOffset()) {
			endX = (int) scroller.getCurrX();
			endY = (int) scroller.getCurrY();
			// logic();
			System.out.println("computeScrollOffset-->" + scroller.getCurrX()
					+ ":" + scroller.getCurrY());
			invalidate();
		}
	}

	/**
	 * 触屏事件监听
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {

		if (event.getAction() == MotionEvent.ACTION_UP) {
			scroller.setFinalX(startX);
			scroller.setFinalX(startY + r);
			System.out.println("up-->" + endX + ":" + endY);
			scroller.startScroll(0, endY, 0, -endY, 2000);
			invalidate();
		} else if (event.getAction() == MotionEvent.ACTION_DOWN) {
			scroller.abortAnimation();

		} else {
			endX = (int) event.getX();
			endY = (int) event.getY();
		}
		return true;
	}

	int contriloffset;
	int contriloffsetx;

	/**
	 * 游戏逻辑
	 */
	private void logic() {
		if (endX != 0 || endY != 0) {
			// 设置操作点为线段x/y的一半
			contriloffset++;
			contriloffsetx++;
			controlX = startX + contriloffsetx;
			controlY = startY + contriloffset;

			control2X = startX - contriloffsetx;
			control2Y = startY + contriloffset;

			if (contriloffset > endY) {
				contriloffset = 0;
			}
			if (contriloffsetx > endX) {
				contriloffsetx = 0;
			}
			// controlX = random.nextInt((endX - startX) / 2);
			// controlY = random.nextInt((endY - startY) / 2);
			dropY = endY;
			r_drop = endY / 20;

		}
	}

	/**
	 * 按键事件监听
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return super.onKeyDown(keyCode, event);
	}

	public void run() {
		while (flag) {
			long start = System.currentTimeMillis();
			myDraw();
			logic();
			long end = System.currentTimeMillis();
			try {
				if (end - start < 50) {
					Thread.sleep(50 - (end - start));
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * SurfaceView视图状态发生改变，响应此函数
	 */
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	}

	/**
	 * SurfaceView视图消亡时，响应此函数
	 */
	public void surfaceDestroyed(SurfaceHolder holder) {
		flag = false;
	}
}
