package com.qpath;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.qpath.PullToRefreshView.OnFooterRefreshListener;
import com.qpath.PullToRefreshView.OnHeaderRefreshListener;

public class MainActivity extends Activity implements OnHeaderRefreshListener,
		OnFooterRefreshListener {
	public static MainActivity instance;
	WaterDropView mWaterDropView;
	private com.qpath.PullToRefreshView mPullToRefreshView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;
		// 设置全屏
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// // 显示自定义的SurfaceView视图
		// mWaterDropView = new WaterDropView(this);
		// setContentView(mWaterDropView);
		setContentView(R.layout.main);
		mWaterDropView = (WaterDropView) findViewById(R.id.waterDropView1);
		mPullToRefreshView = (PullToRefreshView) (findViewById(R.id.pullToRefreshView1));
		mPullToRefreshView.setOnHeaderRefreshListener(this);
		mPullToRefreshView.setOnFooterRefreshListener(this);
		((ListView)findViewById(R.id.list)).setAdapter(new BaseAdapter() {
			
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				// TODO Auto-generated method stub
				TextView mTextView = new TextView(getApplicationContext());
				mTextView.setText("fasdf");
				return mTextView;
			}
			
			@Override
			public long getItemId(int position) {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public Object getItem(int position) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public int getCount() {
				return 100;
			}
		});
	}
	// int y_down = 0;
	// public boolean onTouchEvent(MotionEvent event) {
	// if (event.getAction() == MotionEvent.ACTION_UP) {
	// mWaterDropView.dropFinish();
	// } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
	// mWaterDropView.dropStart();
	// y_down = (int) event.getY();
	//
	// } else {
	//
	// mWaterDropView.droping((int) event.getX(), (int) event.getY()-y_down);
	// }
	// return true;
	// }

	@Override
	public void onFooterRefresh(com.qpath.PullToRefreshView view) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onHeaderRefresh(com.qpath.PullToRefreshView view) {
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				mPullToRefreshView.onHeaderRefreshComplete();
				
			}
		},2000);
		
	}
}