package com.qpath;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Xfermode;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.widget.Scroller;
/**
 * 模拟鼻涕滑落 android仿IOS7 邮件刷新控件
 * @author geliang
 *
 */
public class WaterDropView extends View {
	WaterDropListener mWaterDropListener;

	public void setWaterDropListener(WaterDropListener mWaterDropListener) {
		this.mWaterDropListener = mWaterDropListener;
	}

	public interface WaterDropListener {
		public void onDroping(int heihgt);

		public void onDroped(int heihgt);
	}

	public WaterDropView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension(r * 2, maxDropHigh + r);
		screenW = r * 2;
		screenH = maxDropHigh + r;
		startX = screenW / 2;
		startY = 0;
		// if (re == null) {
		re = new RectF(startX - r, 0, startX + r, r * 2);
		// }
		System.out.println("WaterDropView:" + "onMeasure" + startX + ","
				+ startY);
	}

	/**
	 * 源头的水滴的半径
	 */
	int r = 12;
	int maxDropHigh = 100;

	RectF re;
	Rect re_bitmap;

	private Paint paint;
	// private Thread th;
	// private boolean flag;
	// private Canvas canvas;
	public static int screenW, screenH;
	// -----------以上是SurfaceView游戏框架
	// 贝赛尔曲线成员变量(起始点，控制（操作点），终止点，3点坐标)
	private int startX = r, startY = 0, endX, endY;
	// private int control2X, control2Y,controlX, controlY,;
	// Path
	private Path path;
	private Path path2;
	private Path path_bg;
	// 为了不影响主画笔，这里绘制贝赛尔曲线单独用一个新画笔
	private Paint paintQ;
	// 随机库（让贝赛尔曲线更明显）
	// private Random random;
	Scroller scroller;
	private int color_water = 0xFF28A3E6;
	private int color_bg = Color.TRANSPARENT;
	private int color_progress = 0x22FFFFFF;
	private Bitmap progressBitmap;

	public WaterDropView(Context context) {
		super(context);
		init(context);
	}

	/**
	 * SurfaceView初始化函数
	 * 
	 * @return
	 */
	public void init(Context context) {
		paint = new Paint();
		paint.setColor(color_water);
		paint.setStyle(Style.FILL);
		paint.setAntiAlias(true);
		paint.setDither(true);
		// // 设置光源的方向
		// float[] direction = new float[]{ 1, 1, 1 };
		//
		// //设置环境光亮度
		// float light = 0.4f;
		//
		// // 选择要应用的反射等级
		// float specular = 6;
		//
		// // 向mask应用一定级别的模糊
		// float blur = 3.5f;
		//
		// EmbossMaskFilter emboss=new
		// EmbossMaskFilter(direction,light,specular,blur);
		//
		// // 应用mask
		// paint.setMaskFilter(emboss);
		// paint.setColorFilter(ColorFilter colorfilter);
		// paint.setMaskFilter(new BlurMaskFilter(2,
		// BlurMaskFilter.Blur.SOLID));
		setFocusable(true);
		setClickable(true);
		// 贝赛尔曲线相关初始化
		path = new Path();
		path2 = new Path();
		path_bg = new Path();
		paintQ = new Paint();
		paintQ.setAntiAlias(true);
		// paintQ.setPathEffect(new CornerPathEffect(3));
		paintQ.setPathEffect(new DashPathEffect(new float[] { 100f, 100f }, 0));
		// paintQ.setPathEffect(new DashPathEffect(new float[]{100f,100f},
		// 100f));

		paintQ.setStyle(Style.FILL);
		paintQ.setStrokeWidth(0);
		paintQ.setColor(color_water);
		setModelingShadowLayer(paintQ, 5, 0, 1, 0xFFFFFFFF);
		// random = new Random();
		// scroller = new Scroller(context, new BounceInterpolator(), true);
		scroller = new Scroller(context,
				new AccelerateDecelerateInterpolator(), true);

		if (Build.VERSION.SDK_INT > VERSION_CODES.HONEYCOMB) {
			setLayerType(View.LAYER_TYPE_HARDWARE, null);
		}
		progressBitmap = BitmapFactory.decodeResource(getResources(),
				android.R.drawable.ic_menu_rotate);
		re_bitmap = new Rect(0, 0, progressBitmap.getWidth(),
				progressBitmap.getHeight());
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		// canvas.setDrawFilter(new PaintFlagsDrawFilter(0,
		// Paint.ANTI_ALIAS_FLAG
		// | Paint.FILTER_BITMAP_FLAG));
		if (canvas != null) {
			canvas.drawColor(color_bg);
			// -----------以上是SurfaceView游戏框架

			drawQpath(canvas);
			drawDrop(canvas);
			drawWater(canvas);
		}
	}

	private void setModelingShadowLayer(Paint p, float radius, float dx,
			float dy, int color) {
		if (Build.VERSION.SDK_INT > VERSION_CODES.HONEYCOMB) {
			// p.setShadowLayer(radius, dx, dy, color);
		}

	}

	// /**
	// * 游戏绘图
	// */
	// public void myDraw() {
	// try {
	//
	// canvas = sfh.lockCanvas();
	//
	// } catch (Exception e) {
	// // TODO: handle exception
	// } finally {
	// if (canvas != null)
	// sfh.unlockCanvasAndPost(canvas);
	// }
	// }

	/**
	 * 绘制源头的水
	 * 
	 * @param canvas2
	 */
	private void drawWater(Canvas canvas) {

		// canvas.drawCircle(startX, startY+40,60,paint);
		// canvas.drawCircle(160, 160, 30, paint);
		/* 画一个空心椭圆形 */

		// RectF re=new RectF(startX-r,0,startX+r,r*2+endY/10);
		canvas.drawOval(re, paint);
		canvas.save();
		canvas.rotate(r_drop * 48, re.centerX(), re.centerY());
		canvas.drawBitmap(progressBitmap, re_bitmap, re, paint);
		paint.setColor(r_drop * 48);
		canvas.restore();
		setModelingShadowLayer(paint, 5, 0, -3, color_progress);
		canvas.drawArc(re, 0 + r_drop * 4 - 90, r_drop * 48, true, paint);
		paint.setColor(color_water);
		// paint.setShadowLayer(5, 0, -3, 0x88000000);

		setModelingShadowLayer(paint, 5, 0, -3, 0x88000000);
	}

	int dropY = 0;
	int r_drop;

	/**
	 * 绘制滴下去的水
	 * 
	 * @param canvas2
	 */
	private void drawDrop(Canvas canvas) {
		canvas.drawCircle(startX, dropY, r_drop, paint);

	}

	/**
	 * 绘制贝赛尔曲线
	 * 
	 * @param canvas
	 *            主画布
	 */
	public void drawQpath(Canvas canvas) {

		paintQ.setColor(color_water);
		// paintQ.setShadowLayer( 30 , 2,2,0x00ff0000);
		int offset = endY / 10;
		path_bg.reset();
		path_bg.moveTo(startX - r, r);
		path_bg.lineTo(startX + r, r);
		path_bg.lineTo(startX + (r_drop), dropY);
		path_bg.lineTo(startX - (r_drop), dropY);
		canvas.drawPath(path_bg, paintQ);

		Xfermode xFermode = new PorterDuffXfermode(PorterDuff.Mode.XOR);
		paintQ.setXfermode(xFermode);

		// paintQ.setColor(color_bg);
		paintQ.setColor(0xffffffff);

		// xFermode = new PorterDuffXfermode(PorterDuff.Mode.DST_OVER);
		// paintQ.setXfermode(xFermode);
		// paintQ.setShadowLayer( 30,1,1,0x88000000);
		path.reset();// 重置path
		// path.toggleInverseFillType();
		// 贝赛尔曲线的起始点
		path.moveTo(startX - r, r);
		// 设置贝赛尔曲线的操作点以及终止点
		path.quadTo(startX - r + offset, r + endY / 2, startX - (r_drop), dropY);
		// path.cubicTo(startX-r+offset, r, 160 - 20, endY - 80, 160,
		// 200 + endY / 10);
		// 绘制贝赛尔曲线（Path）
		canvas.drawPath(path, paintQ);

		path2.reset();// 重置path
		// 贝赛尔曲线的起始点
		path2.moveTo(startX + r, r);
		// 设置贝赛尔曲线的操作点以及终止点
		path2.quadTo(startX + r - offset, r + endY / 2, startX + (r_drop),
				dropY);
		// path2.cubicTo(280, 60 + endY / 3, 160 + 20, endY - 80, 160,
		// 200 + endY / 10);
		// 绘制贝赛尔曲线（Path）
		canvas.drawPath(path2, paintQ);

		paintQ.setColor(0xFFFFFFFF);
		canvas.drawLine(startX + r, r, startX + (r_drop), dropY, paintQ);
		canvas.drawLine(startX - r, r, startX - (r_drop), dropY, paintQ);

	}

	@Override
	public void computeScroll() {
		// TODO Auto-generated method stub
		super.computeScroll();

		if (scroller.computeScrollOffset()) {
			endX = (int) scroller.getCurrX();
			endY = (int) scroller.getCurrY();
			// logic();
			System.out.println("computeScrollOffset-->" + scroller.getCurrX()
					+ ":" + scroller.getCurrY());
			invalidate();
			logic();
			if (mWaterDropListener != null) {
				mWaterDropListener.onDroping(scroller.getCurrY());
			}
		} else if (scroller.isFinished()) {
			// if (mWaterDropListener != null) {
			// mWaterDropListener.onDroped(scroller.getCurrY());
			// }

		}
	}

	/**
	 * 触屏事件监听
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// logic();
		System.out.println("onTouch:");
		if (event.getAction() == MotionEvent.ACTION_UP) {
			// scroller.setFinalX(startX);
			// scroller.setFinalX(startY + r);
			// System.out.println("up-->" + endX + ":" + endY);
			// scroller.startScroll(0, endY, 0, -endY, 2000);
			// invalidate();
			dropFinish();
		} else if (event.getAction() == MotionEvent.ACTION_DOWN) {
			// scroller.abortAnimation();
			dropStart();

		} else {
			// endX = (int) event.getX();
			// endY = (int) event.getY();
			droping((int) event.getX(), (int) event.getY());
		}
		// logic();
		// invalidate();
		return false;
//		return true;
	}

	boolean isHasDrop = false;

	/**
	 * the water start drop
	 */
	public void dropStart() {
		// if (!scroller.isFinished()) {
		// return;
		// }
		scroller.abortAnimation();
		logic();
		invalidate();
		isHasDrop = true;
		System.out.println("dropStart" + isHasDrop);
	}

	/**
	 * the water drop down ing
	 * 
	 * @param x
	 *            alaway is 0
	 * @param y
	 *            >=0
	 * 
	 */
	public void droping(int x, int y) {
		if (!scroller.isFinished()) {
			return;
		}
		System.out.println("droping" + isHasDrop);
		if (!isHasDrop) {
			return;
		}
		endX = x;
		endY = y;
		if (endY < 0) {
			endY = 0;
		}
		if (endY > maxDropHigh) {
			endY = maxDropHigh;
			dropFinish();
			return;
		}
		logic();
		invalidate();
	}

	/**
	 * the drop is brance
	 */
	public void dropFinish() {
		System.out.println("dropFinish" + isHasDrop);
		if (!isHasDrop) {
			return;
		}
		scroller.setFinalX(startX);
		scroller.setFinalX(startY + r);
		System.out.println("up-->" + endX + ":" + endY);
		scroller.startScroll(0, endY, 0, -endY, 1000);
		logic();
		invalidate();
		isHasDrop = false;
	}

	/**
	 * the drop is stop
	 */
	public void dropCancl() {
		System.out.println("dropCancl" + isHasDrop);
		if (!isHasDrop) {
			return;
		}
		endX = 0;
		endY = -1;
		logic();
		invalidate();
		isHasDrop = false;
		if (mWaterDropListener != null) {
			mWaterDropListener.onDroped(endY);
		}
	}

	int contriloffset;
	int contriloffsetx;

	/**
	 * 游戏逻辑
	 */
	private void logic() {
		if (endX != 0 || endY != 0) {
			// 设置操作点为线段x/y的一半
			contriloffset++;
			contriloffsetx++;
			// controlX = startX + contriloffsetx;
			// controlY = startY + contriloffset;
			//
			// control2X = startX - contriloffsetx;
			// control2Y = startY + contriloffset;

			if (contriloffset > endY) {
				contriloffset = 0;
			}
			if (contriloffsetx > endX) {
				contriloffsetx = 0;
			}
			// controlX = random.nextInt((endX - startX) / 2);
			// controlY = random.nextInt((endY - startY) / 2);
			dropY = endY;
			r_drop = endY / 16;

		}
	}

	// public void run() {
	// while (flag) {
	// long start = System.currentTimeMillis();
	// myDraw();
	// logic();
	// long end = System.currentTimeMillis();
	// try {
	// if (end - start < 50) {
	// Thread.sleep(50 - (end - start));
	// }
	// } catch (InterruptedException e) {
	// e.printStackTrace();
	// }
	// }
	// }

}
